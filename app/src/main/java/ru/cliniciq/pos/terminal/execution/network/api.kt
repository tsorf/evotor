package ru.cliniciq.pos.terminal.execution.network

import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.http.POST
import ru.cliniciq.pos.terminal.BuildConfig
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp
import ru.frosteye.ovsa.execution.network.client.BaseRetrofitClient
import ru.frosteye.ovsa.execution.network.client.IdentityProvider
import java.security.cert.X509Certificate
import javax.inject.Inject
import javax.net.ssl.*

interface Api {

    @POST("payment/check")
    fun sendPing(): Single<CheckResponse>
}

interface ApiClient {
    val api: Api
}

class RestClient @Inject constructor(

) : BaseRetrofitClient<Api>(
    BuildConfig.BASE_URL
), ApiClient {

    override var identityProvider: IdentityProvider? = null
        get() {
            return ClinicIQApp.appComponent.identityProvider()
        }

    override fun apiClass(): Class<Api> = Api::class.java

    override fun populateOkHttpBuilder(builder: OkHttpClient.Builder) {
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {

            }

            override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {

            }

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }


        })
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        val sslSocketFactory = sslContext.socketFactory

        builder.sslSocketFactory(sslSocketFactory)
        builder.hostnameVerifier { _, _ -> true }
    }
}