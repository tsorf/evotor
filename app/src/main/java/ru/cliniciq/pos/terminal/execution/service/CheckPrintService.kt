package ru.cliniciq.pos.terminal.execution.service

import android.app.Service
import ru.evotor.devices.commons.ConnectionWrapper
import ru.evotor.devices.commons.DeviceServiceConnector
import ru.evotor.devices.commons.services.IPrinterServiceWrapper
import ru.evotor.devices.commons.services.IScalesServiceWrapper
import ru.cliniciq.pos.terminal.data.model.Check
import android.content.Intent
import ru.cliniciq.pos.terminal.presentation.view.impl.activity.PrintActivity


abstract class CheckPrintService : Service(), ConnectionWrapper {

    private var printerService: IPrinterServiceWrapper? = null
    private var scalesService: IScalesServiceWrapper? = null

    override fun onCreate() {
        super.onCreate()
        DeviceServiceConnector.addConnectionWrapper(this)
        DeviceServiceConnector.startInitConnections(applicationContext)
    }

    override fun onScalesServiceConnected(scalesService: IScalesServiceWrapper) {
        this.scalesService = scalesService
    }

    override fun onPrinterServiceConnected(printerService: IPrinterServiceWrapper) {
        this.printerService = printerService
    }

    override fun onPrinterServiceDisconnected() {
        this.printerService = null
    }

    override fun onScalesServiceDisconnected() {
        this.scalesService = null
    }

    protected fun tryToPrint(check: Check): Boolean {
        startActivity(Intent(this, PrintActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(Check::class.java.name, check)
        })
        return true
    }
}