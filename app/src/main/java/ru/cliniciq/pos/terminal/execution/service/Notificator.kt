package ru.cliniciq.pos.terminal.execution.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import ru.cliniciq.pos.terminal.R
import ru.cliniciq.pos.terminal.app.environment.ACTION_START_MAIN
import ru.cliniciq.pos.terminal.presentation.view.impl.activity.SettingsActivity
import ru.frosteye.ovsa.data.storage.ResourceHelper
import ru.frosteye.ovsa.tool.s
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Notificator @Inject constructor(
    private val context: Context
) {

    private val channelId = Notificator::class.java.simpleName

    var notificationId: Int = 1
        private set

    fun createNotification(): Notification {
        createNotificationChannel()
        val notificationIntent = Intent(context, SettingsActivity::class.java)
        notificationIntent.action = ACTION_START_MAIN
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(
            context, 0,
            notificationIntent, 0
        )


        val builder = NotificationCompat.Builder(context, channelId)
            .setContentTitle(ResourceHelper.getString(R.string.app_name))
            .setTicker(ResourceHelper.getString(R.string.app_name))
            .setContentText(ResourceHelper.getString(R.string.app_name))
            .setSmallIcon(getNotificationIcon())
            .setContentIntent(pendingIntent)
            .setOngoing(true)
        return builder.build()
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = R.string.app_name.s()
            val descriptionText = R.string.app_name.s()
            val channel = NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_DEFAULT).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun getNotificationIcon(): Int {
        val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        return if (useWhiteIcon) R.mipmap.ic_launcher else R.mipmap.ic_launcher
    }
}