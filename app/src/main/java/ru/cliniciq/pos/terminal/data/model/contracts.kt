package ru.cliniciq.pos.terminal.data.model

enum class SearchStatus {
    FOUND, NOT_FOUND
}

enum class CheckItemType {
    NORMAL, SERVICE
}