package ru.cliniciq.pos.terminal.execution.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import ru.cliniciq.pos.terminal.app.environment.ACTION_START_SERVICE
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val settings by ClinicIQApp.appComponent.settingsRepo()
        settings?.let {
            if (it.isEnabled) {
                SyncCheckService.action(context!!, ACTION_START_SERVICE)
            }
        }
    }
}