package ru.cliniciq.pos.terminal.presentation.view

import ru.cliniciq.pos.terminal.data.model.AppSettings
import ru.frosteye.ovsa.presentation.view.contract.BasicView


interface SettingsView : BasicView {

    fun showSettings(settings: AppSettings)
}