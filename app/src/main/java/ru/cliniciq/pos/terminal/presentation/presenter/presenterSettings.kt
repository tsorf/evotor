package ru.cliniciq.pos.terminal.presentation.presenter

import android.content.Context
import ru.cliniciq.pos.terminal.app.environment.ACTION_START_SERVICE
import ru.cliniciq.pos.terminal.app.environment.ACTION_STOP_SERVICE
import ru.cliniciq.pos.terminal.data.model.AppSettings
import ru.cliniciq.pos.terminal.data.repo.SettingsRepo
import ru.cliniciq.pos.terminal.execution.service.SyncCheckService
import ru.frosteye.ovsa.presentation.presenter.BasePresenter
import ru.frosteye.ovsa.presentation.presenter.LivePresenter
import ru.cliniciq.pos.terminal.presentation.view.SettingsView
import javax.inject.Inject

interface SettingsPresenter : LivePresenter<SettingsView> {
    fun onSaveSettings(settings: AppSettings)
}

class SettingsPresenterImpl @Inject constructor(
    private val context: Context,
    private val settingsRepo: SettingsRepo
) : BasePresenter<SettingsView>(), SettingsPresenter {

    override fun onAttach(v: SettingsView, vararg params: Any) {
        super.onAttach(v, *params)
        val settings by settingsRepo
        view.showSettings(settings!!)
    }

    override fun onSaveSettings(settings: AppSettings) {
        settingsRepo.save(settings)
        if (settings.isEnabled) {
            SyncCheckService.action(context, ACTION_START_SERVICE)
        } else {
            SyncCheckService.action(context, ACTION_STOP_SERVICE)
        }
    }

}