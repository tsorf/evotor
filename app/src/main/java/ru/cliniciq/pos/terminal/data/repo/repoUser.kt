package ru.cliniciq.pos.terminal.data.repo

import android.content.Context
import ru.cliniciq.pos.terminal.data.model.UserIdentity
import ru.cliniciq.pos.terminal.execution.network.Api
import ru.frosteye.ovsa.data.storage.BaseRepository
import ru.frosteye.ovsa.data.storage.Repository
import ru.frosteye.ovsa.data.storage.Storage
import ru.frosteye.ovsa.execution.network.client.IdentityProvider
import javax.inject.Inject

interface UserRepo : Repository<UserIdentity>, IdentityProvider {

}

class UserRepoImpl @Inject constructor(
    storage: Storage,
    private val api: Api,
    private val context: Context
) : BaseRepository<UserIdentity>(
    storage = storage
), UserRepo {
    override val clazz: Class<UserIdentity> = UserIdentity::class.java

    override fun provideIdentity(): String? = null


}