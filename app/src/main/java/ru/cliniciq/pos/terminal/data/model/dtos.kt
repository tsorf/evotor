package ru.cliniciq.pos.terminal.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import ru.evotor.framework.inventory.ProductItem
import ru.evotor.framework.inventory.ProductType
import ru.evotor.framework.payment.PaymentSystem
import ru.evotor.framework.payment.PaymentType
import ru.evotor.framework.receipt.*
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp
import java.math.BigDecimal

@Parcelize
data class User(
    @SerializedName("id") val id: Int
) : Parcelable

@Parcelize
data class UserIdentity(
    @SerializedName("user") val user: User
) : Parcelable

data class AppSettings(
    @SerializedName("isEnabled") var isEnabled: Boolean = false
)

/*
data class Check(
    @SerializedName("StoreUuId") val store: String,
    @SerializedName("userUuId") val userId: String,
    @SerializedName("deviceUuId") val deviceId: String,
    @SerializedName("paymentSource") val paymentSource: String,
    @SerializedName("dateTime") val dateTime: String,
    @SerializedName("Items") val items: List<CheckItem>,
    @SerializedName("totalTax") val totalTax: Double,
    @SerializedName("totalDiscount") val totalDiscount: Double,
    @SerializedName("totalAmount") val totalAmount: Double
)

data class CheckItem(
    @SerializedName("uuId") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("itemType") val itemType: CheckItemType,
    @SerializedName("measureName") val measureName: String,
    @SerializedName("quantity") val quantity: Int,
    @SerializedName("price") val price: Double,
    @SerializedName("costPrice") val costPrice: Double,
    @SerializedName("sumPrice") val sumPrice: Double,
    @SerializedName("tax") val tax: Double,
    @SerializedName("taxPercent") val taxPercent: Double,
    @SerializedName("discount") val discount: Double
)*/

@Parcelize
data class CheckPosition(
    @SerializedName("productUuid") val productUuid: String,
    @SerializedName("productCode") val productCode: String?,
    @SerializedName("productType") val productType: ProductType,
    @SerializedName("name") val name: String,
    @SerializedName("measureName") val measureName: String,
    @SerializedName("taxNumber") val taxNumber: TaxNumber,
    @SerializedName("price") val price: BigDecimal,
    @SerializedName("quantity") val quantity: BigDecimal
) : Parcelable {

    fun convert(): ProductItem.Product {
        return ProductItem.Product(
            productUuid,
            null,
            productCode,
            name,
            taxNumber,
            productType,
            price,
            quantity,
            null,
            measureName,
            0,
            null, null, null

        )
    }

    fun convertToPosition(): Position {
        return Position.Builder.newInstance(
            convert(), quantity
        ).build()
    }
}

data class CheckWrapper(
    @SerializedName("total") val total: Int,
    @SerializedName("page") val page: Int,
    @SerializedName("pageSize") val pageSize: Int,
    @SerializedName("sortField") val sortField: String?,
    @SerializedName("sortDirection") val sortDirection: String?,
    @SerializedName("data") val data: Check?
)

@Parcelize
data class CheckHeader(
    @SerializedName("clientPhone") val clientPhone: String,
    @SerializedName("clientEmail") val clientEmail: String
) : Parcelable

@Parcelize
data class CheckPrintGroup(
    @SerializedName("identifier") val identifier: String,
    @SerializedName("orgAddress") val orgAddress: String,
    @SerializedName("orgInn") val orgInn: String,
    @SerializedName("orgName") val orgName: String,
    @SerializedName("purchaser") val purchaser: CheckPurchaser,
    @SerializedName("taxationSystem") val taxationSystem: TaxationSystem,
    @SerializedName("type") val type: PrintGroup.Type
) : Parcelable {
    fun convert(): PrintGroup {
        return PrintGroup(
            identifier, type, orgName, orgInn, orgAddress, taxationSystem,
            true, purchaser.convert()
        )
    }
}

@Parcelize
data class CheckPurchaser(
    @SerializedName("name") val name: String,
    @SerializedName("documentNumber") val documentNumber: String,
    @SerializedName("type") val type: PurchaserType?
) : Parcelable {
    fun convert(): Purchaser {
        return Purchaser(name, documentNumber, type)
    }
}

@Parcelize
data class CheckPaymentSystem(
    @SerializedName("paymentSystemId") val paymentSystemId: String,
    @SerializedName("paymentType") val paymentType: PaymentType,
    @SerializedName("userDescription") val userDescription: String?
) : Parcelable {

    fun convert(): PaymentSystem {
        return PaymentSystem(
            paymentType, userDescription ?: "", paymentSystemId
        )
    }
}

@Parcelize
data class CheckPayment(
    @SerializedName("uuid") val uuid: String,
    @SerializedName("value") val value: BigDecimal,
    @SerializedName("paymentSystem") val paymentSystem: CheckPaymentSystem?
) : Parcelable {
    fun convert(): Payment {
        return Payment(
            uuid, value, paymentSystem?.convert(),
            ClinicIQApp.cratePerformer(paymentSystem?.convert()),
            null, null, null
        )
    }
}

@Parcelize
data class Check(
    @SerializedName("header") val header: CheckHeader,
    @SerializedName("uuid") val uuid: String,
    @SerializedName("receiptNumber") val receiptNumber: String,
    @SerializedName("type") val type: Receipt.Type,
    @SerializedName("discount") val discount: BigDecimal,
    @SerializedName("printGroup") val printGroup: CheckPrintGroup,
    @SerializedName("payments") val payments: List<CheckPayment> = mutableListOf(),
    @SerializedName("positions") val positions: List<CheckPosition> = mutableListOf()
) : Parcelable
