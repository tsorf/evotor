package ru.cliniciq.pos.terminal.presentation.view.impl.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.evotor.framework.core.IntegrationException
import ru.evotor.framework.core.IntegrationManagerCallback
import ru.evotor.framework.core.IntegrationManagerFuture
import ru.evotor.framework.core.action.command.print_receipt_command.PrintReceiptCommandResult
import ru.evotor.framework.core.action.command.print_receipt_command.PrintSellReceiptCommand
import ru.evotor.framework.receipt.Payment
import ru.evotor.framework.receipt.Receipt
import ru.cliniciq.pos.terminal.R
import ru.cliniciq.pos.terminal.app.environment.ACTION_STOP_SERVICE
import ru.cliniciq.pos.terminal.data.model.Check
import ru.cliniciq.pos.terminal.execution.service.SyncCheckService
import ru.frosteye.ovsa.tool.common.UITools
import ru.frosteye.ovsa.tool.getParcelable
import java.lang.StringBuilder
import java.math.BigDecimal


class PrintActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print)
        intent.getParcelable<Check>()?.let { check ->
            try {
                /*val map = mutableMapOf<Payment, BigDecimal>()
                check.payments.forEach {
                    map[it.convert()] = it.value
                }
                val receipts = mutableListOf<Receipt.PrintReceipt>(
                    Receipt.PrintReceipt(
                        check.printGroup.convert(),
                        check.positions.map { it.convertToPosition() },
                        map,
                        mapOf(),
                        null
                    )
                )*/

                PrintSellReceiptCommand(
                    check.positions.map { it.convertToPosition() },
                    check.payments.map { it.convert() },
                    check.header.clientPhone, check.header.clientEmail
                )
                    .process(this@PrintActivity, IntegrationManagerCallback {
                        try {
                            val result = it.result
                            when (result.type) {
                                IntegrationManagerFuture.Result.Type.OK -> {
                                    val printSellReceiptResult = PrintReceiptCommandResult.create(result.data)
                                    SyncCheckService.action(this, ACTION_STOP_SERVICE)
                                    UITools.toastLong(this, "OK")
                                }
                                IntegrationManagerFuture.Result.Type.ERROR -> {
                                    UITools.toastLong(this, result.error.message)
                                }
                            }
                        } catch (e: IntegrationException) {
                            e.printStackTrace()
                            UITools.toastLong(this, e.message)
                        }
                        finish()
                    })
            } catch (e: Exception) {
                UITools.toastLong(this, e.message)
                finish()
            }
        }

    }
}
