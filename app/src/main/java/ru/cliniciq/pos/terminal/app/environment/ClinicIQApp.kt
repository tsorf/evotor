package ru.cliniciq.pos.terminal.app.environment

import android.app.Application
import ru.evotor.framework.component.PaymentPerformer
import ru.evotor.framework.payment.PaymentSystem
import ru.cliniciq.pos.terminal.app.di.AppComponent
import ru.cliniciq.pos.terminal.app.di.AppModule
import ru.cliniciq.pos.terminal.app.di.DaggerAppComponent
import android.content.pm.PackageManager
import ru.cliniciq.pos.terminal.R


class ClinicIQApp : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    companion object {

        lateinit var appComponent: AppComponent

        fun cratePerformer(paymentSystem: PaymentSystem?): PaymentPerformer {
            val context = appComponent.context()
            val info = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
            val appUuid = info.metaData.getString("app_uuid")
            return PaymentPerformer(
                paymentSystem,
                context.packageName,
                null,
                appUuid,
                context.getString(R.string.app_name)
            )
        }
    }
}