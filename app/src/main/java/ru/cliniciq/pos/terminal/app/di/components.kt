package ru.cliniciq.pos.terminal.app.di

import android.content.Context
import dagger.Component
import dagger.Subcomponent
import ru.cliniciq.pos.terminal.data.repo.SettingsRepo
import ru.cliniciq.pos.terminal.execution.network.Api
import ru.cliniciq.pos.terminal.execution.service.SyncCheckService
import ru.cliniciq.pos.terminal.presentation.view.impl.activity.*
import ru.cliniciq.pos.terminal.presentation.view.impl.fragment.*
import ru.frosteye.ovsa.di.PresenterScope
import ru.frosteye.ovsa.execution.network.client.IdentityProvider
import javax.inject.Singleton

@Component(modules = [AppModule::class, RepoModule::class])
@Singleton
interface AppComponent {

    operator fun plus(module: PresenterModule): PresenterComponent

    fun identityProvider(): IdentityProvider
    fun api(): Api
    fun context(): Context
    fun inject(service: SyncCheckService)
    fun settingsRepo(): SettingsRepo
}

@Subcomponent(modules = [PresenterModule::class])
@PresenterScope
interface PresenterComponent {

    fun inject(activity: BaseActivity)
    fun inject(activity: SettingsActivity)

    fun inject(fragment: BaseFragment)


}