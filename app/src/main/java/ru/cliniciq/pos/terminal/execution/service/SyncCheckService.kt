package ru.cliniciq.pos.terminal.execution.service

import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import ru.cliniciq.pos.terminal.app.environment.ACTION_START_SERVICE
import ru.cliniciq.pos.terminal.app.environment.ACTION_STOP_SERVICE
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp
import ru.cliniciq.pos.terminal.data.model.Check
import ru.cliniciq.pos.terminal.data.repo.SyncRepo
import ru.frosteye.ovsa.execution.task.toFront
import ru.frosteye.ovsa.tool.common.UITools
import java.lang.Exception
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SyncCheckService : CheckPrintService() {

    @Inject
    lateinit var notificator: Notificator

    @Inject
    lateinit var syncRepo: SyncRepo

    private var isRunning = false
    private var iterator: Disposable? = null
    private var call: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        ClinicIQApp.appComponent.inject(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when(it.action) {
                ACTION_START_SERVICE -> {
                    notify("Service started")
                    if (!isRunning) {
                        isRunning = true
                        startForeground(1, notificator.createNotification())
                        startIterate()
                    }
                }
                ACTION_STOP_SERVICE -> {
                    stop()
                }
            }
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder {
        return object : Binder() {

        }
    }

    private fun stop() {
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        iterator?.dispose()
        call?.dispose()
    }

    private fun startIterate() {
        iterator?.dispose()
        iterator = Observable.interval(5, TimeUnit.SECONDS)
            .toFront()
            .subscribe({
                Log.d("!@#", "SyncService tick")
                check()
            }, {
                it.printStackTrace()
                notify("${it.message} 2")
                stopSelf()
            })
    }

    private fun check() {
        call?.dispose()
        call = syncRepo.check()
            .toFront()
            .subscribe({
                it.pageWrapper?.data?.let {check ->
                    print(check)
                }
            }, {
                notify("${it.message} 1")
            })
    }

    private fun print(check: Check) {
        try {
            tryToPrint(check)
        } catch (e: Exception) {
            e.printStackTrace()
            notify("${e.message} 4")
        }
    }

    private fun notify(message: String?) {
        UITools.toastLong(this, message)
    }

    companion object {

        fun action(context: Context, action: String) {
            Intent(context, SyncCheckService::class.java).apply {
                this.action = action
                context.startService(this)
            }
        }
    }
}
