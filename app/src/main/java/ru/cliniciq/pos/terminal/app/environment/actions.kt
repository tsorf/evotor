package ru.cliniciq.pos.terminal.app.environment

const val ACTION_START_MAIN = "cliniciq.action.main"
const val ACTION_START_SERVICE = "cliniciq.action.start"
const val ACTION_STOP_SERVICE = "cliniciq.action.stop"