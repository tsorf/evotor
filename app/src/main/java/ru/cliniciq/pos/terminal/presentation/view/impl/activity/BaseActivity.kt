package ru.cliniciq.pos.terminal.presentation.view.impl.activity

import android.os.Bundle
import ru.cliniciq.pos.terminal.app.di.PresenterModule
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp
import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity

abstract class BaseActivity : PresenterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val component = ClinicIQApp.appComponent + PresenterModule(this)
        component.inject(this)
        inject(component)
    }
}