package ru.cliniciq.pos.terminal.data.repo

import android.content.Context
import io.reactivex.Single
import ru.cliniciq.pos.terminal.execution.network.Api
import ru.cliniciq.pos.terminal.execution.network.CheckResponse
import ru.frosteye.ovsa.data.storage.*
import ru.frosteye.ovsa.execution.task.toBack
import javax.inject.Inject

interface SyncRepo : CommonRepository {

    fun check(): Single<CheckResponse>
}

class SyncRepoImpl @Inject constructor(
    storage: Storage,
    private val api: Api,
    private val context: Context

) : IndexRepository(storage), SyncRepo {

    override fun check(): Single<CheckResponse> {
        return api.sendPing().toBack()
    }


}