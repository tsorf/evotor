package ru.cliniciq.pos.terminal.presentation.view.impl.fragment

import android.content.Context
import ru.cliniciq.pos.terminal.app.di.PresenterModule
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp
import ru.frosteye.ovsa.presentation.view.fragment.PresenterFragment

abstract class BaseFragment : PresenterFragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val component = ClinicIQApp.appComponent + PresenterModule(this)
        component.inject(this)
        inject(component)
    }
}