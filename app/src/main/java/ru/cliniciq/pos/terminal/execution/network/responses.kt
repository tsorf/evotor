package ru.cliniciq.pos.terminal.execution.network

import com.google.gson.annotations.SerializedName
import ru.cliniciq.pos.terminal.data.model.CheckWrapper


data class CheckResponse(
    @SerializedName("resultCode") var resultCode: Int,
    @SerializedName("resultMessage") var resultMessage: String?,
    @SerializedName("pageWrapper") var pageWrapper: CheckWrapper?,
    @SerializedName("version") var version: String
)