package ru.cliniciq.pos.terminal.execution.network

import com.google.gson.annotations.SerializedName

enum class Os {
    @SerializedName("android")
    ANDROID,
    @SerializedName("ios")
    IOS
}