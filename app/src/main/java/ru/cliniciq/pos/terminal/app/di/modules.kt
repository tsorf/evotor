package ru.cliniciq.pos.terminal.app.di

import android.view.View
import dagger.Module
import dagger.Provides
import ru.cliniciq.pos.terminal.app.environment.ClinicIQApp
import ru.cliniciq.pos.terminal.execution.network.Api
import ru.cliniciq.pos.terminal.execution.network.ApiClient
import ru.cliniciq.pos.terminal.execution.network.RestClient
import ru.frosteye.ovsa.di.PresenterScope
import ru.frosteye.ovsa.di.module.BaseAppModule
import ru.frosteye.ovsa.di.module.BasePresenterModule
import ru.frosteye.ovsa.execution.network.client.IdentityProvider
import ru.cliniciq.pos.terminal.data.repo.*
import ru.cliniciq.pos.terminal.presentation.presenter.*
import ru.cliniciq.pos.terminal.presentation.view.impl.activity.BaseActivity
import ru.cliniciq.pos.terminal.presentation.view.impl.fragment.BaseFragment
import javax.inject.Singleton

@Module
class AppModule(context: ClinicIQApp) : BaseAppModule<ClinicIQApp>(context) {


    @Provides @Singleton fun apiClient(impl: RestClient): ApiClient = impl
    @Provides @Singleton fun api(impl: ApiClient): Api = impl.api
    @Provides @Singleton fun identity(impl: UserRepo): IdentityProvider = impl
}

@Module
class PresenterModule: BasePresenterModule<BaseActivity, BaseFragment> {
    constructor(view: View?) : super(view)
    constructor(activity: BaseActivity?) : super(activity)
    constructor(fragment: BaseFragment?) : super(fragment)

    @Provides @PresenterScope fun main(impl: SettingsPresenterImpl): SettingsPresenter = impl

}

@Module
class RepoModule {

    @Provides @Singleton fun user(impl: UserRepoImpl): UserRepo = impl
    @Provides @Singleton fun settings(impl: SettingsRepoImpl): SettingsRepo = impl
    @Provides @Singleton fun sync(impl: SyncRepoImpl): SyncRepo = impl

}