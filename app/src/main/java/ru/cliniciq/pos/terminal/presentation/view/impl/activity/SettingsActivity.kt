package ru.cliniciq.pos.terminal.presentation.view.impl.activity

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import ru.cliniciq.pos.terminal.R
import ru.cliniciq.pos.terminal.data.model.AppSettings
import ru.cliniciq.pos.terminal.presentation.presenter.SettingsPresenter
import ru.cliniciq.pos.terminal.presentation.view.SettingsView
import javax.inject.Inject

class SettingsActivity : BaseActivity(), SettingsView {

    @Inject
    override lateinit var presenter: SettingsPresenter
    private var isBlocked = false
    private lateinit var settings: AppSettings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun initView(savedInstanceState: Bundle?) {
        settingsStatus.setOnCheckedChangeListener { buttonView, isChecked ->
            settings.isEnabled = isChecked
            presenter.onSaveSettings(settings)
        }
    }

    override fun attachPresenter() {
        presenter.onAttach(this)
    }

    override fun showSettings(settings: AppSettings) {
        this.settings = settings
        blocking {
            settingsStatus.isChecked = settings.isEnabled
        }
    }

    private fun blocking(block: () -> Unit) {
        isBlocked = true
        block()
        isBlocked = false
    }
}
