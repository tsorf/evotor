package ru.cliniciq.pos.terminal.data.repo

import android.content.Context
import ru.cliniciq.pos.terminal.data.model.AppSettings
import ru.frosteye.ovsa.data.storage.BaseRepository
import ru.frosteye.ovsa.data.storage.Repository
import ru.frosteye.ovsa.data.storage.Storage
import javax.inject.Inject

interface SettingsRepo : Repository<AppSettings>

class SettingsRepoImpl @Inject constructor(
    storage: Storage,
    private val context: Context

): BaseRepository<AppSettings>(storage), SettingsRepo {

    override val clazz: Class<AppSettings> = AppSettings::class.java

    override fun handleNull(): AppSettings? {
        return AppSettings()
    }
}